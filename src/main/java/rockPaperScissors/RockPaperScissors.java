package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    Random rand = new Random();
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // PROGRAM START:
        // PRINT SCORE, LOOP FOR VALID INPUT:
        System.out.println("Let's play round " + roundCounter);
        String playerChoice = "";
        String computerChoiceGlob = "";

        while (true) {
            //Player and computer choices:
            String playerInput = readInput("Your choice (Rock/Paper/Scissors)?");
            playerInput = playerInput.toLowerCase();
            String computerChoice = rpsChoices.get(rand.nextInt(3));


            // Check for valid input, else, try again:
            if (rpsChoices.contains(playerInput)){
                playerChoice = playerInput;
                computerChoiceGlob = computerChoice;
                break;
            } else {
                System.out.println("I do not understand " + playerInput + ". Could you try again?");
            }
        }

        //Check for winner:
        if (playerChoice.equals(computerChoiceGlob)) {
            System.out.printf("Human chose %s, computer chose %s. It´s a tie!\n", playerChoice, computerChoiceGlob);
        } else {
            if (playerChoice.equals("rock")){
                if (computerChoiceGlob.equals("paper")) {
                    System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", playerChoice, computerChoiceGlob);
                    computerScore+=1;
                } else {
                    System.out.printf("Human chose %s, computer chose %s. Human wins!\n", playerChoice, computerChoiceGlob);
                    humanScore+=1;
                }
            } else if (playerChoice.equals("paper")) {
                if (computerChoiceGlob.equals("rock")) {
                    System.out.printf("Human chose %s, computer chose %s. Human wins!\n", playerChoice, computerChoiceGlob);
                    humanScore+=1;
                } else {
                    System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", playerChoice, computerChoiceGlob);
                    computerScore+=1;
                }
            } else {
                if (computerChoiceGlob.equals("rock")) {
                    System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", playerChoice, computerChoiceGlob);
                    computerScore+=1;
                } else {
                    System.out.printf("Human chose %s, computer chose %s. Human wins!\n", playerChoice, computerChoiceGlob);
                    humanScore+=1;
                }
            }
        }

        //Continue game, or end it:
        System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);
        String continueGame = readInput("Do you wish to continue playing? (y/n)?");
        if (continueGame.equals("y")){
            roundCounter+=1;
            run();
        } else {
            System.out.println("Bye bye :)");
        }

    }

    /*
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
